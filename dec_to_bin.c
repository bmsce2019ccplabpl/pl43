#include<stdio.h>
#include<math.h>
int input();
int compute(int);
void output(int,int);

int main()
{
    int deci,bin;
    deci=input();
    bin=compute(deci);
    output(deci,bin);
    return 0;
}

int input()
{
    int d;
    printf("Enter the number\n");
    scanf("%d",&d);
    return d;
}

int compute(int d)
{
    int r,b=0;    
    for(int i=0;d!=0;i++)
    {
      r=d%2;
      b=b+r*pow(10,i);
      d=d/2;
    }
    return b; 
}

 void output(int d,int b)
{
    printf("The binary quivalent %d is %d\n",d,b);
}
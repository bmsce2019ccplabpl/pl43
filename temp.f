#include <stdio.h>

float temp(float a)
{
    float c=(32-a)*(5/9);
    return c;
}

int main()
{
    float f, result;
	printf("Enter the Temperature in Fahrenheit\n");
    scanf("%f",&f);
    result=temp(f);
    printf("The temperature in degree celsius is %f\n",result);
    return 0;
}